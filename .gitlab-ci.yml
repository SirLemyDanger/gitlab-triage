stages:
  - prepare
  - test
  - triage
  - deploy

default:
  image: ruby:3.0
  tags:
    - gitlab-org
  cache:
    key:
      files:
        - Gemfile
        - gitlab-triage.gemspec
    paths:
      - vendor/ruby
      - Gemfile.lock
    policy: pull
  before_script:
    - ruby --version
    - gem install bundler --no-document --version 2.0.2
    - bundle --version
    - bundle install --jobs $(nproc) --path=vendor --retry 3 --quiet
    - bundle check

workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For `master` branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'

.use-docker-in-docker:
  image: docker:${DOCKER_VERSION}
  services:
    - docker:${DOCKER_VERSION}-dind
  variables:
    DOCKER_VERSION: "19.03.0"
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
  tags:
    # See https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7019 for tag descriptions
    - gitlab-org-docker

###################
## Prepare stage ##
###################
setup-test-env:
  stage: prepare
  script:
    - echo "Setup done!"
  cache:
    policy: pull-push
  artifacts:
    paths:
      - Gemfile.lock

################
## Test stage ##
################
rubocop:
  stage: test
  needs: ["setup-test-env"]
  dependencies: ["setup-test-env"]
  script:
    - bundle exec rubocop --parallel
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - .cache/rubocop_cache/

# We need to copy this job's definition from the Code-Quality.gitlab-ci.yml
# template because `only` is set without `refs`, so it takes precedence over default workflow rules.
# See https://gitlab.com/gitlab-org/gitlab-ce/issues/66767.
code_quality:
  extends: .use-docker-in-docker
  stage: test
  allow_failure: true
  variables:
    DOCKER_TLS_CERTDIR: ""
  before_script: []
  script:
    - |
      if ! docker info &>/dev/null; then
        if [ -z "$DOCKER_HOST" -a "$KUBERNETES_PORT" ]; then
          export DOCKER_HOST='tcp://localhost:2375'
        fi
      fi
    - docker run
        --env SOURCE_CODE="$PWD"
        --volume "$PWD":/code
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/codequality:12-0-stable" /code
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    expire_in: 1 week

specs:
  needs: ["setup-test-env"]
  stage: test
  script:
    - bundle exec rake spec

##################
## Triage stage ##
##################
dry-run:gitlab-triage:
  needs: ["setup-test-env"]
  stage: triage
  script:
    - bundle exec rake build
    - gem install pkg/*.gem
    - which gitlab-triage
    - gitlab-triage --version
    - gitlab-triage --help
    - gitlab-triage --init
    - gitlab-triage --dry-run --debug --source-id $CI_PROJECT_PATH

# This job requires allows to override the `CI_PROJECT_PATH` variable when triggered.
dry-run:custom:
  extends: dry-run:gitlab-triage
  rules:
    - when: manual
      allow_failure: true

include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - project: 'gitlab-org/quality/pipeline-common'
    file:
      - '/ci/danger-review.yml'
      - '/ci/gem-release.yml'
